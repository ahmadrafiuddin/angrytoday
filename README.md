# AngryToday

AngryToday is a place to vent your frustration of what is really annoying you, what makes you angry or just to say that AngryToday is a bad website. People can "like" and "dislike" posts, and the tags most frequent in posts shows up as a "word cloud".

## To-do

* Fix timestamp issue when adding posts (8 hours behind)
* Implement JWT authentication
* Implement post tag word cloud

## Licensing

AngryToday is licensed under the Internet Systems Consortium License. The full license can be read and found at [https://www.isc.org/downloads/software-support-policy/isc-license/]()

Images used in this project have been obtained from Pixabay. All images and videos on Pixabay are released free of copyrights under Creative Commons CC0. More details can be found at [https://pixabay.com/en/service/faq/]()

## Project structure

All of the source code is found inside the `public` directory.

* `app` - where all the AngularJS code resides
* `content` - CSS, images and other assets
* `vendor` - external libraries used for the project

Custom CSS that are not Bootstrap default's are found in `content/styles.css`. The selectors in that class roughly correspond to the vertical layout of the page (i.e styling for headers at the top and styling for the posts at the bottom).

AngularJS files have been separated into three directories, `controllers`, `directives` and `services`. I have tried my best to adhere to John Papa's [AngularJS style guide](https://github.com/johnpapa/angular-styleguide/blob/master/a1/README.md) in organising my code.

"Posts" are obtained by `postsService` found in `app.postsService.js`. Temporarily, it performs an AJAX request from `angerPosts.json` found inside the `content` directory to supply the application with posts. When/if a REST API is built, this file should be modified.

## How to run

To run this project, you must first install `nodejs` on your computer. On Linux, this usually means finding and installing the `nodejs` package using your distribution's package manager. Details can be found [here](https://howtonode.org/how-to-install-nodejs). Afterwards, open bash/cmd and `cd` to the project directory (the same one containing this README file).

Install the project dependencies

```
npm install
```

Start AngryToday by running `npm run serve` and it will fire up your web browser automatically.

```bash
$ npm run serve
```

Note that the page will not be able to retrieve any posts if you open `index.html` directly without the use `express`, this is due to a pathing issue (relative & absolute). Also note that location lookup functionality will not work without internet access (requires Google Maps JavaScript API).
