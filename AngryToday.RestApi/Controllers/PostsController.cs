﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AngryToday.RestApi.DAL;
using AngryToday.RestApi.Extensions;
using AngryToday.RestApi.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.Extensions.Options;

namespace AngryToday.RestApi.Controllers
{
    [Produces("application/json")]
    [Route("api/posts")]
    public class PostsController : Controller
    {
        private readonly AngryTodayContext _context;
        private readonly SiteSettingsConfiguration _siteSettings;

        public PostsController(AngryTodayContext context, IOptions<SiteSettingsConfiguration> siteSettings)
        {
            _context = context;
            _siteSettings = siteSettings.Value;
        }

        // GET: api/posts
        [HttpGet]
        public IEnumerable<Post> GetPosts()
        {
            return _context.Posts.Include(q => q.Author);
        }

        // GET: api/posts/recent
        [HttpGet("recent")]
        public IEnumerable<Post> GetPostsByTime()
        {
            return _context.Posts.Include(q => q.Author).OrderByDescending(p => p.Updated);
        }

        // GET: api/posts/likes
        [HttpGet("likes")]
        public IEnumerable<Post> GetPostsByLikes()
        {
            return _context.Posts.Include(q => q.Author).OrderByDescending(p => p.Likes);
        }

        // GET: api/posts/7ad251f2-6152-ac3e-4fc21-2121cca399fe
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPost([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var post = await _context.Posts.Include(q => q.Author).SingleOrDefaultAsync(p => p.Id == id);

            if (post == null)
            {
                return NotFound();
            }

            return Ok(post);
        }

        [HttpPost("{id}/like")]
        public async Task<IActionResult> LikePost([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var post = await _context.Posts.Include(q => q.Author).SingleOrDefaultAsync(p => p.Id == id);

            if (post == null)
            {
                return NotFound();
            }

            post.Likes += 1;
            post.Liked = true;
            await _context.SaveChangesAsync();

            var model = new
            {
                likes = post.Likes
            };

            return Ok(model);
        }

        [HttpPost("{id}/dislike")]
        public async Task<IActionResult> DislikePost([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var post = await _context.Posts.Include(q => q.Author).SingleOrDefaultAsync(p => p.Id == id);

            if (post == null)
            {
                return NotFound();
            }

            post.Dislikes += 1;
            post.Disliked = true;
            await _context.SaveChangesAsync();

            return Ok();
        }

        // PUT: api/posts/7ad251f2-6152-ac3e-4fc21-2121cca399fe
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPost([FromRoute] Guid id, [FromBody] Post post)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != post.Id)
            {
                return BadRequest();
            }

            _context.Entry(post).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PostExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/posts
        [HttpPost]
        public async Task<IActionResult> PostPost([FromBody] Post post)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var guid = Guid.NewGuid();
            post.Id = guid;
            post.Link = StringHelpers.CombineUrl(_siteSettings.HostUrl, "/post/" + guid);

            if (post.Author == null)
            {
                post.Author = new Author
                {
                    Name = "Anonymous",
                    Email = ""
                };
            }

            if (String.IsNullOrWhiteSpace(post.Author.Name))
            {
                post.Author.Name = "Anonymous";
            }

            _context.Posts.Add(post);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPost", new { id = post.Id }, post);
        }

        // PATCH: api/posts/7ad251f2-6152-ac3e-4fc21-2121cca399fe
        [HttpPatch("update")]
        public async Task<IActionResult> PatchPost([FromRoute] Guid id, [FromBody] JsonPatchDocument<Post> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var post = await _context.Posts.Include(q => q.Author).SingleOrDefaultAsync(p => p.Id == id);
            var originalPost = post;

            if (post == null)
            {
                return NotFound();
            }

            patch.ApplyTo(post, ModelState);
            _context.SaveChanges();

            var model = new
            {
                original = post,
                updated = post
            };

            return Ok(model);
        }


        // DELETE: api/posts/7ad251f2-6152-ac3e-4fc21-2121cca399fe
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePost([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var post = await _context.Posts.SingleOrDefaultAsync(m => m.Id == id);
            if (post == null)
            {
                return NotFound();
            }

            _context.Posts.Remove(post);
            await _context.SaveChangesAsync();

            return Ok(post);
        }

        private bool PostExists(Guid id)
        {
            return _context.Posts.Any(e => e.Id == id);
        }
    }
}