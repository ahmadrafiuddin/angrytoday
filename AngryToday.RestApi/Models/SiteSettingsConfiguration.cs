﻿namespace AngryToday.RestApi.Models
{
    public class SiteSettingsConfiguration
    {
        public string HostUrl { get; set; }
    }
}
