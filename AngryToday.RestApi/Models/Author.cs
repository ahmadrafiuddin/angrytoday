﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace AngryToday.RestApi.Models
{
    public class Author
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}