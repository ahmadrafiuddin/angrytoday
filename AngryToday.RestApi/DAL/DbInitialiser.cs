﻿using System.Linq;
using AngryToday.RestApi.Models;

namespace AngryToday.RestApi.DAL
{
    public static class DbInitialiser
    {
        public static void Initialise(AngryTodayContext dbContext)
        {
            // If DB does not exist, create DB
            dbContext.Database.EnsureCreated();

            if (dbContext.Authors.Any())
            {
                return; // DB has been seeded
            }

            var authors = new Author[]
            {
                new Author { Name = "Syazwana Rafitra", Email = "syaz@hotmail.com" },
                new Author { Name = "Ashley Teo", Email = "ashley@groundmail.com" },
                new Author { Name = "Amirul Arif", Email = "mirul@groundmail.com" },
                new Author { Name = "Amir Bakri", Email = "amir@supermail.com" },
                new Author { Name = "Michael Goh", Email = "michael.goh@gmail.com" },
                new Author { Name = "Krishnan Subramaniam", Email = "kris@tutamail.com" },
            };

            foreach (var author in authors)
            {
                dbContext.Authors.Add(author);
            }
            dbContext.SaveChanges();

            var posts = new Post[]
            {
                new Post
                {
                    Body = "Lecturers who can't even come to class should just quit their job.",
                    Location = "Skudai, Johor",
                    Longitude = "1.56477",
                    Latitude = "103.637259",
                    Author = dbContext.Authors.First(q => q.Email == "syaz@hotmail.com"),
                    Likes = 12,
                    Dislikes = 3,
                    Liked = false,
                    Disliked = false
                },
                new Post
                {
                    Body = "Why is this train so slow?",
                    Location = "Skudai, Johor",
                    Longitude = "1.56477",
                    Latitude = "103.637259",
                    Author = dbContext.Authors.First(q => q.Email == "ashley@groundmail.com"),
                    Likes = 3,
                    Dislikes = 0,
                    Liked = false,
                    Disliked = false
                },
                new Post
                {
                    Body = "This training could definitely have been done better.",
                    Location = "Skudai, Johor",
                    Longitude = "1.56477",
                    Latitude = "103.637259",
                    Author = dbContext.Authors.First(q => q.Email == "mirul@groundmail.com"),
                    Likes = 10,
                    Dislikes = 5,
                    Liked = false,
                    Disliked = false
                },
                new Post
                {
                    Body = "Nullam vehicula enim elit, nec condimentum metus rhoncus quis. Phasellus nisl neque, consectetur sollicitudin pharetra eu, bibendum in orci. Nam faucibus efficitur felis, quis dignissim erat semper vestibulum. Duis sodales ultricies finibus. Vestibulum velit augue, faucibus nec porttitor maximus, fermentum non diam. Duis non pharetra massa. Integer mattis ornare iaculis. Vivamus commodo mauris sed felis hendrerit, vitae ullamcorper dolor ultricies.",
                    Location = "Skudai, Johor",
                    Longitude = "1.56477",
                    Latitude = "103.637259",
                    Author = dbContext.Authors.First(q => q.Email == "amir@supermail.com"),
                    Likes = 20,
                    Dislikes = 3,
                    Liked = false,
                    Disliked = false
                },
                new Post
                {
                    Body = "Nullam vehicula enim elit, nec condimentum metus rhoncus quis. Phasellus nisl neque, consectetur sollicitudin pharetra eu, bibendum in orci. Nam faucibus efficitur felis, quis dignissim erat semper vestibulum. Duis sodales ultricies finibus. Vestibulum velit augue, faucibus nec porttitor maximus, fermentum non diam. Duis non pharetra massa. Integer mattis ornare iaculis. Vivamus commodo mauris sed felis hendrerit, vitae ullamcorper dolor ultricies.",
                    Location = "Putrajaya, Wilayah Persekutuan",
                    Longitude = "2.943242",
                    Latitude = "101.673489",
                    Author = dbContext.Authors.First(q => q.Email == "michael.goh@gmail.com"),
                    Likes = 9,
                    Dislikes = 21,
                    Liked = false,
                    Disliked = false
                },
                new Post
                {
                    Body = "Nullam vehicula enim elit, nec condimentum metus rhoncus quis. Phasellus nisl neque, consectetur sollicitudin pharetra eu, bibendum in orci. Nam faucibus efficitur felis, quis dignissim erat semper vestibulum. Duis sodales ultricies finibus. Vestibulum velit augue, faucibus nec porttitor maximus, fermentum non diam. Duis non pharetra massa. Integer mattis ornare iaculis. Vivamus commodo mauris sed felis hendrerit, vitae ullamcorper dolor ultricies.",
                    Location = "Port Dickson, Negeri Sembilan",
                    Longitude = "2.943242",
                    Latitude = "101.673489",
                    Author = dbContext.Authors.First(q => q.Email == "kris@tutamail.com"),
                    Likes = 4,
                    Dislikes = 9,
                    Liked = false,
                    Disliked = false
                },
            };

            foreach (var post in posts)
            {
                dbContext.Posts.Add(post);
            }
            dbContext.SaveChanges();
        }
    }
}