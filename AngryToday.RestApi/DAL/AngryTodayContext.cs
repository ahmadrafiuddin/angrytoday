﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AngryToday.RestApi.Models;
using Microsoft.EntityFrameworkCore;

namespace AngryToday.RestApi.DAL
{
    public class AngryTodayContext : DbContext
    {
        public AngryTodayContext(DbContextOptions options) : base(options) { }

        public DbSet<Post> Posts { get; set; }
        public DbSet<Author> Authors { get; set; }
    }
}
