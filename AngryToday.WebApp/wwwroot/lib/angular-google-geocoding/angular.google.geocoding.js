angular.module('angular.google.geocoding',[])
.provider('$googleGeocoding',function(){
   var httpConfiguration = {
    cache: true
   };

   var requestConfiguration = {
      language: 'en'
   };

   this.httpConfig = function(opts){ angular.extend(httpConfiguration, opts ); }
   this.config = function(opts){ angular.extend(requestConfiguration, opts ); }
   var componentsFiltering = function(args){
    if(args.components){

    }

   }
   
   this.$get = ["$http", "$q", function($http, $q){
      

      return {
        geocode: function(args){
           if( angular.isString(args)){
             args = { address: args }
           }
           return this._makeRequest(args);
        },
        reverse: function(args){
          return this.reverseGeocode(args);
        },
        reverseGeocode: function(args){
          if(angular.isObject(args) && args.latitude !== undefined && args.longitude !== undefined){
            args.lat = args.latitude;
            args.lng = args.longitude;
          }

          if( angular.isArray(args) && args.length == 2){
            args = { latlng: args.join(",") }
          }else if(angular.isObject(args) && args.lat !== undefined && args.lng !== undefined){
            args = { latlng: this._flattenCoords(args) }
          }
          return this._makeRequest(args);

        },
        _flattenCoords: function(coords){
          return [coords.lat, coords.lng].join(",");
        },
        _processBounds: function(args){
          if(args.bounds && args.bounds.northeast && args.bounds.southwest && angular.isObject(args.bounds)){
            args.bounds = [this._flattenCoords(args.bounds.southwest), this._flattenCoords(args.bounds.northeast)].join("|");
          }
        },
        _processComponents: function(args){
          for(var key in args){
            if(angular.isArray(args[key]) && 
              (key == 'components' || key == "result_type"|| key == "location_type"))
                args[key] = args[key].join("|");
          }      
        }, 
        _makeRequest: function(args){
          this._processComponents(args);
          this._processBounds(args);
          var config = angular.extend(angular.copy(httpConfiguration), {params: angular.extend(angular.copy(requestConfiguration), args ) });
          var deferred = $q.defer();
          $http.get(
            "https://maps.googleapis.com/maps/api/geocode/json",
            config
          ).then(function(response){
              deferred.resolve(response.data)
          }, function(response) {
              deferred.reject(response)
          });

          return deferred.promise
        },
        components:{
          "route":"route",
          "locality":"locality",
          "administrative_area":"administrative_area",
          "country":"country",
          "postal_code":"postal_code",
        }, 
        status : {
          "OK" : "OK",
          "ZERO_RESULTS": "ZERO_RESULTS",
          "OVER_QUERY_LIMIT": "OVER_QUERY_LIMIT",
          "REQUEST_DENIED": "REQUEST_DENIED",
          "INVALID_REQUEST": "INVALID_REQUEST",
          "UNKNOWN_ERROR": "UNKNOWN_ERROR",
        },
        location_type : {
          "ROOF_TOP" : "ROOF_TOP",
          "RANGE_INTERPOLATED": "RANGE_INTERPOLATED",
          "APPROXIMATE": "APPROXIMATE",
          "GEOMETRIC_CENTER": "GEOMETRIC_CENTER",
        }
      }
   }]
});
