Angular Google Geocoding
===================


**Installation**
-------

**Bower**
```sh
bower install angular-google-geocoding
```
**Meteor**
```sh
meteor add lucavandro:angular-google-geocoding
```

**Configuration**
-------
```js
angular.module("app",["angular.google.geocoding"])
.config(function($googleGeocodingProvider){
     $googleGeocoding.httpConfig({
         "cache": false //Default true
     });

	$googleGeocoding.config({
	     "key": "YOUR_API_KEY", // OPTIONAL
         "language": "it",      // OPTIONAL
         "bounds": {            // OPTIONAL
               "northeast" : {
                  "lat" : 34.2355209,
                  "lng" : -118.5534191
               },
               "southwest" : {
                  "lat" : 34.1854649,
                  "lng" : -118.588536
               }
        }
     });
});
```
**$googleGeocoding.httpConfig(config)** takes an object describing the request to be made and how it should be processed.
For a full list check the [AngularJS docs](https://docs.angularjs.org/api/ng/service/$http#usage)

**$googleGeocoding.config(config)** takes an object describing the parameters to use for each geocoding request.
For a full list check the [Google Maps Geocoding API](https://developers.google.com/maps/documentation/geocoding/intro#geocoding) and [Google Maps Reverse Geocoding API](https://developers.google.com/maps/documentation/geocoding/intro#ReverseGeocoding) .

----------

**Usage**
-------

```js
angular.module("app",['angular.google.geocoding'])
	   .controller("YourCtrl", function($scope,$googleGeocoding){
			$googleGeocoding
			.geocode('Casagiove, CE, Italy')
			.then(function(response){ console.log(response )});
			
			$googleGeocoding
			.reverseGeocode({ lat: 40.714224, lng: -73.961452})
			.then(function(response){ console.log(response )});
			
			// Very handy when dealing with navigator.currentPosition
			$googleGeocoding
			.reverseGeocode({ latitude: 40.714224, longitue: -73.961452})
			.then(function(response){ console.log(response )});
			
			// Array will work as well
			$googleGeocoding
			.reverseGeocode([40.714224,-73.961452])
			.then(function(response){ console.log(response )});
			
			// .reverse is to .reverseGeocode as "hi" is to "hello" 
			$googleGeocoding
			.reverse({ lat: 40.714224, lng: -73.961452})
			.then(function(response){ console.log(response )});
		})
```