Package.describe({
  name: 'lucavandro:angular-google-geocoding',
  version: '1.0.5',
  // Brief, one-line summary of the package.
  summary: 'Angular provider for Google Geocoding API',
  // URL to the Git repository containing the source code for this package.
  git: 'https://github.com/lucavandro/angular-google-geocoding.git',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.1.0.2');
  api.use("angular@1.3.0");
  api.addFiles([
      'angular.google.geocoding.js'
  ]);
});
