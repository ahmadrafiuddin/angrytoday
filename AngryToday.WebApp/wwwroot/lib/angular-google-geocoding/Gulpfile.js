
var gulp = require('gulp');
var uglify = require('gulp-uglify');
var rename = require("gulp-rename");

gulp.task('compress', function() {
  return gulp.src('angular.google.geocoding.js')
    .pipe(uglify())
    .pipe(rename('angular.google.geocoding.min.js'))
    .pipe(gulp.dest('.'));
});

gulp.task('default', ['compress'], function() {
  // place code for your default task here
});
