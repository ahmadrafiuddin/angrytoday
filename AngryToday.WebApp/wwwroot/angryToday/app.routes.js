(function () {
    'use strict';

    angular
        .module('angryToday')
        .config(configureStates);

    function configureStates($stateProvider) {
        var states = [
            {
                name: 'home',
                url: '',
                template: '<angrytoday-app></angrytoday-app>'
            },
            {
                name: 'home2',
                url: '/',
                template: '<angrytoday-app></angrytoday-app>'
            },
            {
                name: 'home.post',
                url: '/post/{postId}',
                onEnter: function ($uibModal, $state, $stateParams) {
                    var postId = $stateParams.postId;

                    $uibModal.open({
                        templateUrl: 'angryToday/post-grid/post/post-modal.view.html',
                        controller: PostModalCtrl,
                        controllerAs: '$ctrl',
                        size: 'lg',
                        resolve: {
                            postId: function() { return postId; }
                        }
                    }).result.then(function () {
                        // Success
                        $state.go('home');
                    }, function () {
                        // Cancel
                        $state.go('home');
                    });
                }
            }
        ];

        states.forEach(function (state) {
            $stateProvider.state(state);
        });
    }

    function PostModalCtrl(postId, postsService) {
        var $ctrl = this;
        
        postsService.getPost(postId).then(function (post) {
            $ctrl.postData = post;
        });
    }

})();
