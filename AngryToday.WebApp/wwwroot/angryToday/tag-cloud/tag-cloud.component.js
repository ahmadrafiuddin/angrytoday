(function() {
  'use strict';

  // Usage:
  //
  // Creates:
  //

  angular.module('angryToday').component('tagCloud', {
    templateUrl: 'angryToday/tag-cloud/tag-cloud.component.html',
    controller: TagCloudController,
    controllerAs: '$ctrl',
    bindings: {
      Binding: '='
    }
  });

  TagCloudController.$inject = [];
  function TagCloudController() {
    var $ctrl = this;

    // TODO: Develop REST API endpoint for tags
    $ctrl.tags = [
      {
        tag: 'angry',
        weight: 43
      },
      {
        tag: 'blood',
        weight: 3
      },
      {
        tag: 'people',
        weight: 4
      },
      {
        tag: 'upset',
        weight: 9
      },
      {
        tag: 'phone',
        weight: 16
      },
      {
        tag: 'car',
        weight: 12
      },
      {
        tag: 'crash',
        weight: 5
      }
    ];
    ////////////////

    $ctrl.$onInit = function() {
      $(document).ready(function() {
        $('#tag-cloud a').tagcloud({
          size: {
            start: 3,
            end: 4,
            unit: 'vw'
          },
          color: {
            start: '#cde',
            end: '#f52'
          }
        });
      });
    };
    $ctrl.$onChanges = function(changesObj) {};
    $ctrl.$onDestroy = function() {};
  }
})();
