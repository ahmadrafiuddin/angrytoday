(function() {
  'use strict';

  // Usage:
  //
  // Creates:
  //

  angular.module('angryToday').component('angrytodayApp', {
    templateUrl: 'angryToday/app.component.html',
    controller: AppComponentController,
    controllerAs: '$ctrl',
    bindings: {
      Binding: '='
    }
  });

  AppComponentController.$inject = [];
  function AppComponentController() {
    var $ctrl = this;

    ////////////////

    $ctrl.$onInit = function() {};
    $ctrl.$onChanges = function(changesObj) {};
    $ctrl.$onDestroy = function() {};
  }
})();
