(function () {
    'use strict';

    // Usage:
    //
    // Creates:
    //

    angular.module('angryToday').component('postGrid', {
        templateUrl: 'angryToday/post-grid/post-grid.component.html',
        controller: PostGridController,
        controllerAs: '$ctrl',
        bindings: {
            Binding: '='
        }
    });

    PostGridController.$inject = ['postsService'];
    function PostGridController(postsService) {
        var $ctrl = this;

        $ctrl.posts = null;

        ////////////////

        $ctrl.sortByRecent = function () {
            postsService.getPosts().then(function (posts) {
                $ctrl.posts = posts;
                $ctrl.isSortedByRecent = true;
            });
        };
        $ctrl.sortByPopularity = function () {
            postsService.getPostsByLikes().then(function (posts) {
                $ctrl.posts = posts;
                $ctrl.isSortedByRecent = false;
            });
        };

        $ctrl.refreshPosts = function () {
            if ($ctrl.isSortedByRecent) {
                $ctrl.sortByRecent();
            } else {
                $ctrl.sortByPopularity();
            }
        }

        $ctrl.$onInit = function () {
            $ctrl.isSortedByRecent = true;
            postsService.getPosts().then(function (posts) {
                $ctrl.posts = posts;
            });
        };
        $ctrl.$onChanges = function (changesObj) { };
        $ctrl.$onDestroy = function () { };
    }
})();
