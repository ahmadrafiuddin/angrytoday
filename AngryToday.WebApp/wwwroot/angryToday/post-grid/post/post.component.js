(function () {
    'use strict';

    // Usage:
    //
    // Creates:
    //

    angular.module('angryToday').component('post', {
        // template:'htmlTemplate',
        templateUrl: 'angryToday/post-grid/post/post.component.html',
        controller: postController,
        controllerAs: '$ctrl',
        bindings: {
            postData: '<'
        }
    });

    postController.$inject = ['postsService'];
    function postController(postsService) {
        var $ctrl = this;

        ////////////////

        $ctrl.like = function(postId) {
            $ctrl.liked = true;
            postsService.likePost(postId).then(function() {
                $ctrl.likes += 1;
            });
        };

        $ctrl.dislike = function(postId) {
            $ctrl.disliked = true;
            postsService.dislikePost(postId).then(function() {
                $ctrl.dislikes += 1;
            });
        };

        $ctrl.$onInit = function () {
            $ctrl.id = $ctrl.postData.id;
            $ctrl.name = $ctrl.postData.author.name;
            $ctrl.location = $ctrl.postData.location;
            $ctrl.email = $ctrl.postData.author.email;
            $ctrl.body = $ctrl.postData.body;
            $ctrl.photoUrl = $ctrl.postData.photoUrl;
            $ctrl.likes = $ctrl.postData.likes;
            $ctrl.dislikes = $ctrl.postData.dislikes;
            $ctrl.updated = $ctrl.postData.updated;
        };
        $ctrl.$onChanges = function (changesObj) { };
        $ctrl.$onDestroy = function () { };
    }
})();
