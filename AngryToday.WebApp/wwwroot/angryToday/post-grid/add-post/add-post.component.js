(function () {
    'use strict';

    // Usage:
    //
    // Creates:
    //

    angular.module('angryToday').component('addPost', {
        templateUrl: 'angryToday/post-grid/add-post/add-post.component.html',
        controller: AddPostController,
        controllerAs: '$ctrl',
        bindings: {
            refreshPosts: '&'
        }
    });

    AddPostController.$inject = ['geolocation', '$googleGeocoding', 'postsService'];
    function AddPostController(geolocation, $googleGeocoding, postsService) {
        var $ctrl = this;
        
        ////////////////

        $ctrl.submitAnger = function (formData) {
            var angerPost = $.extend(formData, $ctrl.userLocation);
            console.log(angerPost);
            postsService.addPost(angerPost).then(function() {
                console.log("Post added");
                $ctrl.refreshPosts();
            });
        };

        $ctrl.expandPostBody = function () {
            $('textarea.anger-textarea').animate({
                "height": "5.5em",
                "margin-bottom": "10px"
            }, 500, function () {
                // on finish animation
                $('div.post-controls').slideDown(500);
            });
        };

        $ctrl.$onInit = function () {
            $ctrl.loadingLocation = true;
            $ctrl.userLocation = { location: "Ghost Valley, Unknown" };
            $('div.post-controls').hide();

            geolocation
                .getLocation()
                .then(
                function (posObj) {
                    var lat = posObj.coords.latitude;
                    var lng = posObj.coords.longitude;
                    
                    $googleGeocoding
                        .reverseGeocode({
                            lat: lat,
                            lng: lng
                        })
                        .then(function (response) {
                            var results = response.results;
                            var city, state;

                            for (var i = 0; i < results[0].address_components.length; i++) {
                                for (var j = 0; j < results[0].address_components[i].types.length; j++) {
                                    // there are different types that might hold a city 
                                    // admin_area_lvl_1 usually does in come cases looking for sublocality type 
                                    // will be more appropriate
                                    if (results[0].address_components[i].types[j] == "locality") {
                                        //this is the object you are looking for
                                        city = results[0].address_components[i];
                                    }

                                    if (results[0].address_components[i].types[j] == "administrative_area_level_1") {
                                        //this is the object you are looking for
                                        state = results[0].address_components[i];
                                    }

                                    if (city && state) {
                                        // once city & state has been found, break loop
                                        break;
                                    }
                                }
                            }
                            $ctrl.loadingLocation = false;
                            $ctrl.userLocation = {
                                latitude: lat,
                                longitude: lng,
                                location: city.long_name + ", " + state.long_name
                            };
                        });
                },
                function (error) {
                    console.log("Error: " + error);
                    $ctrl.loadingLocation = false;
                });
        };
        $ctrl.$onChanges = function (changesObj) { };
        $ctrl.$onDestroy = function () { };
    }
})();
