(function () {
  'use strict';

  var appModule = angular.module('angryToday',
    [
      'ui.router',
      'masonry',
      'ngtimeago',
      'geolocation',
      'angular.google.geocoding',
      'angularModalService',
      'ui.bootstrap'
    ]);

  appModule.value('apiBaseUrl', 'http://localhost:3633/api/');

})();
