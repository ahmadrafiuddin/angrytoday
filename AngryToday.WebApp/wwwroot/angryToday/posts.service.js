(function () {
    'use strict';

    angular
        .module('angryToday')
        .service('postsService', postsService);

    postsService.$inject = ['$http', 'apiBaseUrl'];
    function postsService($http, apiBaseUrl) {
        this.getPosts = getPosts;
        this.getPostsByLikes = getPostsByLikes;
        this.getPost = getPost;
        this.addPost = addPost;
        this.likePost = likePost;
        this.dislikePost = dislikePost;

        ////////////////

        function getPosts() {
            var api_url = apiBaseUrl + 'posts/recent';
            return $http.get(api_url)
                .then(function success(result) {
                    return result.data;
                }, function error(result) {
                    console.log("PostsService: Error executing getAllPosts()");
                    console.log(result);
                });
        }
        function getPostsByLikes() {
            var api_url = apiBaseUrl + 'posts/likes';
            return $http.get(api_url)
                .then(function success(result) {
                    return result.data;
                }, function error(result) {
                    console.log("PostsService: Error executing getPostsByLikes()");
                    console.log(result);
                });
        }

        function getPost(postId) {
            var api_url = apiBaseUrl + 'posts/' + postId;
            return $http.get(api_url)
                .then(function success(result) {
                    return result.data;
                }, function error(result) {
                    console.log("PostsService: Error executing getPost(postId)");
                    console.log(result);
                });
        }

        function addPost(postData) {
            var api_url = apiBaseUrl + 'posts';
            return $http.post(api_url, postData)
                .then(function success(result) {
                    return result;
                },
                function error(result) {
                    console.log("PostsService: Error executing addPost(postData)");
                    console.log(result);
                });
        }

        function likePost(postId) {
            var api_url = apiBaseUrl + 'posts/' + postId + '/like';
            return $http.post(api_url)
                .then(function success(result) {
                    return result;
                },
                function error(result) {
                    console.log("PostsService: Error executing likePost(postId)");
                    console.log(result);
                });
        }

        function dislikePost(postId) {
            var api_url = apiBaseUrl + 'posts/' + postId + '/dislike';
            return $http.post(api_url)
                .then(function success(result) {
                    return result;
                },
                function error(result) {
                    console.log("PostsService: Error executing likePost(postId)");
                    console.log(result);
                });
        }
    }
})();