(function () {
    angular
        .module('app')
        .service('postsService', postsService);

    function postsService($http, $document) {
        var angerPosts = [];

        return {
            getPosts: getPosts,
            sortByRecent: sortByRecent,
            sortByPopularity: sortByPopularity,
            rebuildMasonryGrid: rebuildMasonryGrid,
            addPost: addPost
        };

        function getPosts(callback) {
            if (angerPosts.length === 0) {
                $http({
                    method: 'GET',
                    url: '/content/angerPosts.json'
                }).then(function successCallback(response) {
                    angerPosts = response.data;
                    callback(angerPosts);
                }, function errorCallback(response) {
                    console.log("Error: Failed to obtain posts");
                    callback(angerPosts);
                });
            }
            callback(angerPosts);
        }

        function sortByRecent() {
            angerPosts = angerPosts.sort(function (a, b) {
                return (a.updated < b.updated) ? 1 : ((a.updated > b.updated) ? -1 : 0);
            });
            $document.find('.btn-sort-popularity').removeClass('active');
            $document.find('.btn-sort-recent').addClass('active');
            rebuildMasonryGrid();
        }

        function sortByPopularity() {
            angerPosts = angerPosts.sort(function (a, b) {
                return b.likes - a.likes;
            });
            $document.find('.btn-sort-recent').removeClass('active');
            $document.find('.btn-sort-popularity').addClass('active');
            rebuildMasonryGrid();
        }

        function rebuildMasonryGrid() {
            setTimeout(function () {
                var $grid = $('#post-grid');
                $grid.masonry('reloadItems');
                $grid.masonry();
            }, 100);
        }

        function addPost(post) {
            if (!post.author.name) {
                post.author.name = "Anonymous";
            }
            post.updated = new Date().toISOString();
            post.identity = angerPosts[0].identity + 1;
            angerPosts.unshift(post);
            sortByRecent();
        }
    }
})();