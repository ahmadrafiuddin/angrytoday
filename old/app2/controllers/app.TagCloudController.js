(function () {
    'use strict';

    angular
        .module('app')
        .controller('TagCloudController', TagCloudController);

    function TagCloudController() {
        /* jshint validthis: true */
        var vm = this;

        init(vm);

        vm.tags = [{
                "tag": "angry",
                "weight": 43
            },
            {
                "tag": "blood",
                "weight": 3
            },
            {
                "tag": "people",
                "weight": 4
            },
            {
                "tag": "upset",
                "weight": 9
            },
            {
                "tag": "phone",
                "weight": 16
            },
            {
                "tag": "car",
                "weight": 12
            },
            {
                "tag": "crash",
                "weight": 5
            }
        ];

        ////////////////

        function init(vm) {
            $(document).ready(function () {
                $('#tag-cloud a').tagcloud({
                    size: {
                        start: 3,
                        end: 4,
                        unit: 'vw'
                    },
                    color: {
                        start: '#cde',
                        end: '#f52'
                    }
                });
            });
        }
    }
})();