(function () {
    'use strict';

    angular
        .module('app')
        .controller('AngerFormController', AngerFormController);

    function AngerFormController(postsService, geolocation, $googleGeocoding) {
        /* jshint validthis: true */
        var vm = this;

        vm.loadingLocation = true;
        vm.formData = {
            title: "",
            link: "",
            identity: "",
            updated: "",
            body: "",
            photo_url: "",
            location: {
                latitude: "",
                longitude: "",
                name: "Ghost Valley, Unknown"
            },
            author: {
                name: "",
                email: ""
            },
            likes: 0,
            dislikes: 0,
            liked: false,
            disliked: false
        };

        vm.expandTextbox = expandTextbox;
        vm.submitAnger = submitAnger;

        init();

        ////////////////

        function resetForm() {
            vm.formData = {
                title: "",
                link: "",
                identity: "",
                updated: "",
                body: "",
                photo_url: "",
                location: {
                    latitude: "",
                    longitude: "",
                    name: "Ghost Valley, Unknown"
                },
                author: {
                    name: "",
                    email: ""
                },
                likes: 0,
                dislikes: 0,
                liked: false,
                disliked: false
            };
        }

        function expandTextbox() {
            $('textarea.anger-textarea').animate({
                "height": "5.5em",
                "margin-bottom": "10px"
            }, 500, function () {
                // on finish animation
                $('div.post-controls').slideDown(500);
            });
        }

        function submitAnger(formData) {
            if (vm.angerForm.$valid) {
                postsService.addPost(formData);

                // clear form fields
                resetForm();
            }
        }

        function init() {
            $('div.post-controls').hide();

            geolocation
                .getLocation()
                .then(
                    function (posObj) {
                        var lat = posObj.coords.latitude;
                        var lng = posObj.coords.longitude;

                        vm.formData.location.latitude = lat;
                        vm.formData.location.longitude = lng;

                        $googleGeocoding
                            .reverseGeocode({
                                lat: lat,
                                lng: lng
                            })
                            .then(function (response) {
                                var results = response.results;
                                var city, state;

                                for (var i = 0; i < results[0].address_components.length; i++) {
                                    for (var j = 0; j < results[0].address_components[i].types.length; j++) {
                                        // there are different types that might hold a city 
                                        // admin_area_lvl_1 usually does in come cases looking for sublocality type 
                                        // will be more appropriate
                                        if (results[0].address_components[i].types[j] == "locality") {
                                            //this is the object you are looking for
                                            city = results[0].address_components[i];
                                        }

                                        if (results[0].address_components[i].types[j] == "administrative_area_level_1") {
                                            //this is the object you are looking for
                                            state = results[0].address_components[i];
                                        }

                                        if (city && state) {
                                            // once city & state has been found, break loop
                                            break;
                                        }
                                    }
                                }
                                vm.loadingLocation = false;
                                vm.formData.location = {
                                    latitude: lat,
                                    longitude: lng,
                                    name: city.long_name + ", " + state.long_name
                                };
                            });
                    },
                    function (error) {
                        console.log("Error: " + error);
                        vm.loadingLocation = false;
                    }
                );
        }
    }
})();