(function () {
    'use strict';

    angular
        .module('app')
        .controller('ScrollController', ScrollController);

    function ScrollController($window) {
        var vm = this;

        vm.scrollToTop = scrollToTop;

        init();

        ////////////////

        function scrollToTop() {
            var scrollDuration = 300;
            
            $('html, body').animate({scrollTop: 0}, scrollDuration);
            $('a.back-to-top').fadeOut(fadeDuration);
        }

        function init() {
            $(document).scroll(function () {
                var pixelsScrolled = 250;
                var fadeDuration = 300;

                if ($(this).scrollTop() > pixelsScrolled) {
                    $('a.back-to-top').fadeIn(fadeDuration);
                } else {
                    $('a.back-to-top').fadeOut(fadeDuration);
                }
            });
        }
    }
})();