(function () {
    'use strict';

    angular
        .module('app')
        .controller('DisplayPostsController', DisplayPostsController);

    function DisplayPostsController(postsService) {
        /* jshint validthis: true */
        var vm = this;

        vm.posts = [];
        vm.sortByRecent = sortByRecent;
        vm.sortByPopularity = sortByPopularity;
        vm.openModal = openModal;
        vm.like = like;
        vm.dislike = dislike;

        init();

        ////////////////

        function init() {
            postsService.getPosts(function callback(posts) {
                vm.posts = posts;

                $(document).ready(function () {
                    $('#post-grid').imagesLoaded().progress(function () {
                        $('#post-grid').masonry({
                            "itemSelector": ".post",
                            "columnWidth": ".post-grid-sizer",
                            "percentPosition": true
                        });
                    });
                    $('.btn-sort-recent').addClass('active');
                });
            });
        }

        function sortByRecent() {
            vm.monthFilter = '';
            postsService.sortByRecent();
        }

        function sortByPopularity() {
            vm.monthFilter = new Date().toISOString().substring(0, 7);
            postsService.sortByPopularity();
        }

        function openModal(post) {
            $('#' + post.identity).modal('show');
        }

        function like(post) {
            if (post.disliked) {
                post.disliked = false;
                post.dislikes -= 1;
            }
            if (!post.liked) {
                post.likes += 1;
                post.liked = true;
            } else {
                post.likes -= 1;
                post.liked = false;
            }
        }

        function dislike(post) {
            if (!post.disliked) {
                post.disliked = true;
                post.dislikes += 1;
            } else {
                post.disliked = false;
                post.dislikes -= 1;
            }
            if (post.liked) {
                post.likes -= 1;
                post.liked = false;
            }
        }
    }
})();