/**
 * @desc anger post directive that represents one AngryToday post
 * @example <anger-post></anger-post>
 */

(function() {
    'use strict';

    angular
        .module('app')
        .directive('angerPost', angerPost);

    function angerPost() {
        var directive = {
            restrict: 'E',
            templateUrl: './content/post.html'
        };
        return directive;
    }
})();