(function () {
    'use strict';

    angular.module('app', [
        'ngtimeago',
        'geolocation',
        'angular.google.geocoding'
    ]);
})()